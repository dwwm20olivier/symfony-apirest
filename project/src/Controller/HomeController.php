<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;

class HomeController extends AbstractController
{
    #[Route('/home', name: 'home')]
    public function index(UserRepository $user): Response
    {
        $users = $user->findAll();
        return $this->render('home/index.html.twig', [
            'users' => $users
        ]);
    }

    #[Route('/user', name:'create_user')]
    public function create(Request $request,EntityManagerInterface $em,UserPasswordHasherInterface $encode)
    {
        $user = new User();
        $form = $this->createForm(UserType::class,$user);
        $form->handleRequest($request);
        if( $form->isSubmitted() ){
            $pass = ($form->getData()->getPassword());
            $user->setPassword($encode->hashPassword($user,$pass));
            $user->setRoles(['ROLE_USER']);
            $em->persist($user);
            $em->flush();
            return new JsonResponse($user);
        }
        return $this->render('home/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
#[route('/admin',name:'admin')]
    public function admin()
    {
        return $this->render('home/admin.html.twig', [
            
        ]);
    }
}
